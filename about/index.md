+++
title = "About me"
date = "2022-10-19T15:20:21Z"
author = "devve"
authorTwitter = "" #do not include @
cover = ""  # "images/me.png"
tags = ["me", "biography", "description", "personal", "objective", "about", "studies", "career"]
keywords = ["biography", "bio", "about", "myself", "personal", "description", "studies", "career"]
description = "Get to know a bit more about me!"
showFullContent = false
readingTime = true
hideComments = false
+++

{{< image src="images/me.png" alt="Memoji of my face" position="center" style="height: 220px;" >}}

Hi, I'm Jaime, but my nerdy buddies call me devve and so can you.
I am a software engineer with passion for good design. I currently live in
Madrid, freelancing while studying. I have helped build startups from the
ground up, such as MOVO (finally part of Cabify) and Wannalisn.

I love FOSS (free open-source software). Btw, I use Debian 😉.

### Education

* Telecommunication Engineering student [at] Universidad Politécnica de
Madrid. Major in Telematics.

* Statistical-Computational Processing of Information student [at] Universidad
Complutense de Madrid.


### Interests

In my free time, I like to play guitar, shoot film photography and play some sports. I also love motorcycles, and I am a happy owner of one of these happiness devices.

### Fun facts

* My memory is really bad.
* I also have two other happiness devices, but they are cats.
* I can't draw.
* I don't like justified text.
* My sister can't tell whether I am joking or not.
* I like uncomfortable situations. 

I am curious about everything. Drop me a line if I can be helpful.
