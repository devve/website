+++
title = "Contact me"
date = "2022-10-19T18:13:49Z"
author = "devve"
authorTwitter = "" #do not include @
cover = ""
tags = ["contact", "email", "mastodon", "pixelfed", "social networks"]
keywords = ["contact", "email", "mastodon", "pixelfed", "social networks"]
description = "Places to reach me"
showFullContent = false
readingTime = true
hideComments = false
+++

- Email: [jaimeconde@etik.com](mailto:jaimeconde@etik.com)
- Mastodon: [@devve](https://mstdn.social/@devve)
- XMPP: [devve@conversations.im](xmpp:devve@conversations.im)
- Codeberg: [devve](https://codeberg.org/devve)
- Github: [d3vv3](https://github.com/d3vv3) (only mirrors from Codeberg)
