+++
title = "You own nothing"
date = "2022-11-14T01:30:00Z"
author = "devve"
authorTwitter = "" #do not include @
cover = "images/drm.png"
tags = ["opinion", "subscription", "ownership", "warranty", "rent", "e-book"]
keywords = ["opinion", "subscription", "ownership", "warranty", "rent", "e-book"]
description = "You bought your phone, right? Do you consider you own it? Or do you often see yourself not doing something with it to keep it on warranty? Would you like to replace the screen yourself? Why can't you? This and much more covered on this article."
showFullContent = false
readingTime = true
hideComments = false
+++

> This article is tagged with `#opinion`, and thus you might not agree with me.

When I was a kid, Microsoft Office 2005 was a thing. If some classmate sent me
a `.doc` from Microsoft Office 2007 there would be some incompatibility around
the corner somewhere. Thing is, years went by and you still had full access to
your Microsoft Office 2005 program. It was yours. Same with Photoshop CS4 to 6
. Now, if you cancel your 19.99 € monthly subscription to Adobe Creative Cloud,
good luck opening your `.psd` files.

> In fact, they recently removed **colors** (Pantone) from Photoshop, even if
> your `.psd` files are old! Do I have to pirate colors now?
> {{< mastodon "https://mstdn.social/@devve/109248579194437459" >}}

Of course I switched to their Free Open Source counterparts such as
<mark>OnlyOffice</mark>, <mark>Gimp</mark> and <mark>Inkscape</mark>.

This world is now ruled by monthly subscriptions to everything. You are most
likely renting a lot of things you would say you own, but you really don't own
almost anything.

But, what about the things you buy for real? The so called one-time purchases?
Big Corps have been pulling every play on their book to keep you hooked to
their services for as long as they can, attempting to squeeze as much revenue
from you as possible. Hell, restrictive warranties from phone manufacturers is
starting to feel like you don't own your phone. You replaced the screen
yourself? Well, now it is out of warranty. Why aren't there original
replacement parts available? You got a third-party replacement? If they really
cared for the environment they would show some support for local workshops or
self-service repairments instead of removing the charger from the box.

> Yes, we removed the charger from the box. Use the ones you already have! But
if the battery is swollen, no doubt they will dismiss the warranty request on
the grounds of the charger not being the original.

> In this article I will exclude things I consider as renting, such as Netflix,
Spotify and others. I consider I own nothing on this platforms and I am renting
my access to those monthly.



### DRM

{{< figure src="images/drm.png" alt="Phone, CD and another phone handcuffed and chained" position="center" style="border-radius: 8px;" caption="Illustration by Brendan Mruk [CC BY-SA 3.0]" captionPosition="right" captionStyle="color: green;" >}}

Let's tackle the elephant in the room: DRM or Digital Rights Management is
there to protect the content being delivered to the end consumer. You will see
this if you try to take a screenshot when watching Netflix on your phone
(content should appear black). It is there to prevent piracy, but it is also
there to avoid the end user owning what they just bought.

Some years ago (and some still remains) one would buy a Blu-Ray movie and have
a physical item they could enjoy at their house. It was also protected against
piracy with some RIP protections, but it was yours. The physical part of it
made it yours. Same occurred with software: it would come in a box with a CD,
license code and installation/usage manual. Now that those are gone, it is
starting to feel like NFTs. In the digital era, copies of an item are free and
can be infinite (up to the demand, really), while Blu-Ray's were limited on
quantity (and there were manufacturing costs).

Taking it further: let's take a book you just bought on Amazon. You can, after
searching for a while, download the `.mobi`, `.epub` or `.kff` file. Well, it
turns out that this file is still not yours. You can't get this file to your
tablet to read it there. This is because Amazon provides your devices and 
account with a license to use that book (and unlocks it for you on your
devices, but it won't do that outside their platform). So you are out of luck
if you want a PDF.

It isn't the first time I find myself doing some piracy to get the PDF out of
the book I just bought.

