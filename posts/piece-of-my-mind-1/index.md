+++
title = "Piece of my mind #1: engineered serendipity"
date = "2023-03-22T22:11:00Z"
author = "devve"
authorTwitter = "" #do not include @
cover = "images/serendipity.png"
tags = ["opinion", "experiences", "serendipity", "friendship", "friends", "travel"]
keywords = ["", ""]
description = "The same feeling arises every March-April, coinciding with spring. Why does it feel this way? Why do I remember how alive I have been... now?"
showFullContent = false
readingTime = true
hideComments = false
+++

> This article is tagged with `#opinion`, and thus you might not agree with me.

My heart is full right now. It is on fire. But I don't want it to stop. I want
to keep this feeling, I want to maximize it. Everything else feels muted now:
meetings, academics, routine... It is all empty. I have shared this days with
friends from the good old days on a trip and I relived my 16s to 18s. It was
still there, it wasn't me ageing and maturing or changing priorities. It was 
that I moved into other surroundings that are muting me. People I am
surrounded by are not lighting my heart; pushing me forward with experiences 
and moments. I mean, sometimes they do, but it is not as constant as it was 
then. And I only have 4 hometown friends, but damn they are hard to beat.

7 years ago I was 16. I was on highschool and everything was f____ng perfect.
I had my 4 (literally 4) hometown friends. We went to class together, study
together, learning to play guitar together, work on projects, skipped classes
together... Motorbike licenses and new motorbikes started coming and made it
all much easier and enjoyable. Some of 18s came by and we started getting our
car driving licenses: roadtrips, excursions... Academics were always easy and
fun to do on group: evenings in the library literally just laughing, doing
mostly nothing; studying for university access exam and so on.

> I know the brain tends to forget the bad stuff and keep only the highlights,
> creating that feeling of the good old days. But I already knew at that time,
> I was living my prime time. That sensation did not come with time after
> forgetting the bad moments. I shared this thoughts with some friends, who
> also find 16s to 18s to be their best years by far.

University came by and I sought excellence, moved to another city and kept my
academics by the book. My friends moved to another city. Over the course of
this 7 years, I have not been able to find people like my hometown
friends. Not like I did not maximize my chances of doing so; I did. I joined
student associations, traveled around the world, had a wonderful year on
2019/20 (my 20s), and another wonderful 2021/22 (my 22s). If I found this
people, I have not been able to keep them around. On my first year at
university, my dad told me my hometown school friends would always be the ones
you keep for the rest of your life. To this day, neither of us can explain why
this happens, but damn is it true.

During this 7 years, I noticed something about how I behave with people. I
enjoy meeting new people, learning and having experiences from and with them.
Once that starts fading out, I disconnect easily. Let's throw a graph here:

{{< figure src="images/log.png" alt="Log graphs showing how much experiences or knowledge gain they give me, ideal being the diagonal: y=x with no asymptote" position="center" style="border-radius: 8px;" caption="Definitely not my best graph" captionPosition="left" captionStyle="color: green;" >}}

Sorry for the messy plot, give it a minute. On mode, I hold people
around for 1 or 2 years. Then, once it intercepts the yellow ideal line, my
interests shift towards someone still on the left of the diagonal (yellow
line). Some people are short lived on the left, others are long lived, and
someone could even be ideal. I have a friend who I keep learning from since I
know him (6 years now). I don't behave this way on purpose, but I observed it
is how my brain works. I like measuring and understanding.

One of my hometown friends recently posted about engineered serendipity. It is
the idea of maximizing the chances for those opportunities to take place. Sort
of "I don't get lucky, I make my own luck", but without the arrogance.

{{< figure src="images/serendipity.png" alt="A four leaves clover as a dotted pattern like in a Montecarlo's Pi approximation" position="center" style="border-radius: 8px;" caption="Illustration by @drex_jpg on Instagram" captionPosition="right" captionStyle="color: green;" >}}

It reminds me of the Montecarlo approximation for Pi but without the square.
Back to the picture, it is possible to increase your serendipity surface area
and engineer your own luck. Your daily habits can put you in a position where
"luck" is more likely to strike. Taking it home with the use of maths, if you
live in a 2 dimension world, you are less likely to find this. Otherwise, if
you add someone orthogonal (each person is a dimension, different to yours,
even though you share the origin 0), you will end up in a 3 dimensional space,
or n dimensional. I want to find those people again; and I want them to be a
diagonal. If that makes sense.

{{< image src="images/orthogonal.jpg" alt="3d axes" position="center" style="height: 400px;">}}

Obviously you cannot connect with an unlimited n of people, so you have to
evaluate each axis and dimension with care and compatibility in mind. Dunbar's
number is still a limitation. Some axis will make you grow towards something
you love and enjoy, others won't: you will prune those.

You may think this is very cold, but so is death; which is a very natural
process. Things have a beginning and an end, it is what it is. And it is not
just me. Most people are experiencing the same with me. I am another log curve
in their lives, a new axis in their Rn world they might not be interested in.
And that is fine.

Concluding something, if there is something to conclude; this feeling I so
associate with spring, at the end of March or beginning of April, gives me the
sensation of being alive. It burns. In the finding of this feeling, I got my
first motorbike as a vehicle to adventures, people and others; and I will
continue to seek it.

> About this feeling: If I feel it too often, will it compress the signal?
> Will it be normalized over time, so it won't ever be the same?
