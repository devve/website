+++
title = "It's time: let's talk decentralized"
date = "2022-11-05T00:30:00Z"
author = "devve"
authorTwitter = "" #do not include @
cover = "images/cover.png"
tags = ["opinion", "fediverse", "federation", "mastodon", "pixelfed", "ActivityPub"]
keywords = ["opinion", "fediverse", "federation", "mastodon", "pixelfed", "ActivityPub"]
description = "Let's try to walk you through the fediverse, how federation works and the world it enables. Hopefully you can find your little community online!"
showFullContent = false
readingTime = true
hideComments = false
+++

> While reeding, keep in mind this article is tagged as `#opinion`.  
> I may tend to illustrate stuff with mastodon, since it is the most advanced
> network in the fediverse.


## Concept

The concept is not difficult. In fact, <mark>you already know it as email</mark>. When you send
an email, you are reaching `someone@somewhere.thingy`. It can be `gmail.com`,
`yahoo.com`, `autistici.org` or whatever. When gmail is down, yahoo users can still
send and receive emails.

Currently, when you create an account on Instagram, you are `@user(@instagram.com)`,
but the last part is skipped since there is no other place where Instagram would
check if an account exists. The same happens with Twitter.

Now imagine that Instagram, Twitter or YouTube worked as the email, because there were
different servers where the users could be registered on: `user1@mastodon.social`,
`user2@fosstodon.social`, `user1@mastodon.art`.

The last part of this users is the instance they are registered in. The server. If
every user registered on the same instance, the fediverse wouldn't make sense, since
a single node would concentrate all the network.

{{< peertube host="framatube.org" id="4294a720-f263-4ea4-9392-cf9cea4d5277" title="What is the Fediverse?" >}}

> Decentralized networks are resilient to censorship. If one platform "cancels" you,
> you can simply pick up and move to another server. If one server goes down, the
> rest remain.

All Fediverse servers are completely free and open source software, which means
that anyone can build their own community.


## Instances

Now comes where it gets interesting. Every server has its own mindset, its little
community of alike users. Some are more general, some are LGTBIQ+, some are tech
oriented, some are art oriented, mainly for one language, and so on.

> There are thousands of "servers" on the Fediverse. Each is totally unique with their
> own rules, themes, and even features. Most servers federate with most others.
> Choose a community that is right for you, and move at any time.

{{< figure src="images/mastodon.png" alt="Community of mastodon welcoming a new user" position="center" style="border-radius: 8px;" caption="Illustration by dopatwo" captionPosition="right" captionStyle="color: green;" >}}

For example, the admin of your instance may have blocked fascist servers. It is
common for users to first register in a generic instance and then migrate their
account to an instance that fits them better. And that is totally fine!

But you can still interact with other instances (unless they are banned by your
admin)! There will be three timelines:
1. The timeline you created by following certain users.
2. Local timeline: with the activity of your instance.
3. Federated timeline: activity of your instance and second order connections from
other instances.

## How does the federation take place

But how do they talk from one instance to another? The email world is driven by
protocols such as IMAP and SMTP, DNS entries (SPF, TXT, DMARK). Here, the fediverse
is driven by the *ActivityPub* (and its underlyings). I won't go further here.

{{< image src="images/activity-pub.png" alt="Instances interconnected through the ActivityPub" position="center" style="border: none; height: 400px;">}}


## Supporting your admin

I donate monthly to the admin of my instance. Mostly these donations just cover the
infrastructure expenses and invoices are shared (at least on my instance). When the
expenses are not being covered, the admin shares a toot calling for help and support.

You don't have to donate, but they do not run ads and they host the instance out of
passion.

## What is out there?

There are a few federated alternatives to commonly known services, such as:
* [Mastodon](https://joinmastodon.org/): Twitter-like
* [Pixelfed](https://pixelfed.org/): Instagram-like
* [PeerTube](https://joinpeertube.org/): YouTube-like
* [Owncast](https://owncast.online/): Twitch-like
* [Write Freely](https://writefreely.org/): Wattpad-like
* More coming! The admin of my mastodon instance is developing a TikTok-like
federated social network.

{{< figure src="images/branches.webp" alt="Tree with federated services categorized by type" position="center" style="border-radius: 8px;" caption="Illustration by Per Axbom" captionPosition="right" captionStyle="color: green;" >}}

## Apps

Each federated social network has its own set of apps, even though some are
cross-compatible (normally between pixelfed and mastodon). You can have a look
at mastodon apps [here](https://joinmastodon.org/apps).

There are also apps that support most of the social networks of the fediverse
into one app.

## How is the people?

If there is something I can say, it is that the interactions I had into the
fediverse have been much richer than on Twitter/Instagram. They <mark>add value and
are not toxic</mark>, since people on an instance are much more alike. It also helps that
the instance decides what servers it will not federate with, avoiding most of the
toxic interactions.

## Considerations

1. In an ideal decentralized world, as an university student you may join your
university nodes for mastodon, pixelfed and others, and then take your account
elsewhere once your studies are finished or you find a better place.

2. People tend to disaggregate themselves into existing instances that fit them and no
mechanisms of protection against centralization of network power need to be
implemented (like in blockchain).

3. Currently, many users are migrating from Twitter and, therefore, a lot of instances
have a waiting list. Admins are not a big corporate that can scale up the servers
when needed. If you are interested, have some patience!

More information about the fediverse can be found [here](https://jointhefedi.com/).

{{< mastodon "https://mastodont.cat/@fediverse/109290893179984552" >}}
