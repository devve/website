+++
title = "Using your AirTag as a bluetooth speaker"
date = "2022-10-26T23:30:00Z"
author = "devve"
authorTwitter = "" #do not include @
cover = "images/airtag.jpg"
tags = ["airtag", "tracking", "speaker", "apple", "silent", "objective", "clickbait"]
keywords = ["airtag", "tracking", "speaker", "apple", "silent", "objective"]
description = "Imagine using the speakers in your AirTag as a bluetooth speaker. Well that would be interesting but we are not doing that. Instead I will cover how to remove the speaker on an AirTag so you can track your vehicle without it making noise every so often."
showFullContent = false
readingTime = true
hideComments = false
+++

Of course the title is clickbait. Even if you were able to play music through it, the
speaker is quite tiny. I am guessing it uses a resonating frequency to make it sound
loud enough.

I have an AirTag on my motorbike. Whenever my iPhone is not close, it beeps quite loud.
The thing is: I don't have an iPhone! I have an iPad and it is always quite far from the
motorbike.

Of course, the beeping is there on purpose. It would only beep in case the owner left
something behind, allowing for people around to notice it. If that wasn't the case,
the sound would allow someone to know they have an AirTag with them, sharing their
location.

> Please never use an AirTag on something that is not yours.

I actually have two locators on my bike:
- One is motorbike specific and is hidden in plain sight under the seat, allowing 
the thief to disconnect it and feel successful in some way.
- The AirTag, hidden away. This is the fallback locator in case they disconnect and
leave the first one behind.

Now I will provide the steps I followed, than also happen to be the ones that will
leave your AirTag as good as possible, without looking battle-scarred.

## Steps

1. First twist the back metal plate and take out the battery.
{{< image src="images/open.jpg" alt="Back metal plate, battery and AirTag from left to right" position="center" style="border-radius: 8px; margin-top: 20px;" >}}

2. With a knife or thin blade, slowly vandalize the three places where there are
tabs. You might have a look at step 4 to better hint the key points.
{{< image src="images/opened.jpg" alt="From left to right: AirTag board and midplate" position="center" style="border-radius: 8px; margin-top: 20px;" >}}

3. Notice the speaker on the reverse of the plastic midplate. Now take that knife and
split the midplate and the speaker apart.
{{< image src="images/opened_reverse.jpg" alt="From left to right: AirTag board and midplate with speaker attached" position="center" style="border-radius: 8px; margin-top: 20px;" >}}
{{< image src="images/opened_speaker_removed.jpg" alt="From left to right: AirTag board, midplate and speaker separated" position="center" style="border-radius: 8px; margin-top: 20px;" >}}

4. Leave the speaker out and close the midplate by aligning the battery contact points
and applying pressure.
{{< image src="images/closed.jpg" alt="Closed AirTag" position="center" style="border-radius: 8px; margin-top: 20px;" >}}

5. Put it back together and wait for no sound to tell you it is on.

As you can see, steps are simple enough, even though on step 2 I managed to also
vandalize my finger.
